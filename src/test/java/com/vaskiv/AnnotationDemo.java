package com.vaskiv;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@interface SmartPhone{
      String OS () default "new10s";
      int version() default 1;
}

@SmartPhone(OS = "Iphone", version = 6)
class iPhone{
    String model;
    int size;
    public iPhone(String model, int size) {
        this.model = model;
        this.size = size;
    }
}

public class AnnotationDemo {
    public static void main(String [] args){
        iPhone obj = new iPhone("Fire",5);
         Class c = obj.getClass();
        Annotation an = c.getAnnotation(SmartPhone.class);
        SmartPhone s = (SmartPhone)an;
        System.out.println(s.OS());

    }
}
